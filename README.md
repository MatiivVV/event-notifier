# Social network event notifier  
  
** Functionality: **  
  
+ Receives application event messages from social network  
+ Persists messages to database  
+ Sends notification emails on schedule  
  
** Tools: **  
Back end: JDK 11, Spring 5.2.6, Spring Boot 2.3.0, JMS 1.1 / ActiveMQ 5.15.12, Spring Data MongoDB 3.0.0, Jakarta Mail 1.6.5, Lombok 1.18.12, JUnit 5.6.2, Mockito 3.3.3, Embedded MongoDB 2.2.0, MongoDB 4.2.7, Maven 3.6.3, Git / Bitbucket, IntelliJIDEA 2019.3.4  
  
_  
**Matiiv Vasily**  
Training getJavaJob  
http://www.getjavajob.com  