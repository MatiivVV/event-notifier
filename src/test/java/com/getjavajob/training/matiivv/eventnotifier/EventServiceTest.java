package com.getjavajob.training.matiivv.eventnotifier;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class EventServiceTest {

    @Autowired
    private EventService eventService;
    private EventService eventServiceSpy;
    @MockBean
    private EventRepository eventRepository;
    @MockBean
    private JavaMailSender javaMailSender;
    private Object[] mocks;
    private Event event1 = new Event("event1");
    private Event event2 = new Event("event2");

    @BeforeEach
    void setUp() {
        eventServiceSpy = spy(eventService);
        eventService.setEventService(eventServiceSpy);
        eventService.setDestinations(List.of("destination1", "destination2"));
        eventService.setSender("sender");
        mocks = new Object[]{eventRepository, javaMailSender, eventServiceSpy};
    }

    @Test
    void testReceiveEvent() {
        when(eventRepository.save(eq(event1))).thenReturn(event1);
        eventService.receiveEvent("event1");
        verify(eventRepository).save(eq(event1));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    void testProcessEventsExist() {
        when(eventRepository.findBySentFalse()).thenReturn(List.of(event1, event2));
        doNothing().when(eventServiceSpy).processEvent(any(Event.class));
        eventService.processEvents();
        verify(eventRepository).findBySentFalse();
        verify(eventServiceSpy, times(2)).processEvent(any(Event.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    void testProcessEventsExistFailed() {
        when(eventRepository.findBySentFalse()).thenReturn(List.of(event1, event2));
        doThrow(RuntimeException.class).when(eventServiceSpy).processEvent(eq(event1));
        doNothing().when(eventServiceSpy).processEvent(eq(event2));
        eventService.processEvents();
        verify(eventRepository).findBySentFalse();
        verify(eventServiceSpy, times(2)).processEvent(any(Event.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    void testProcessEventsNotExist() {
        when(eventRepository.findBySentFalse()).thenReturn(List.of());
        doNothing().when(eventServiceSpy).processEvent(any(Event.class));
        eventService.processEvents();
        verify(eventRepository).findBySentFalse();
        verifyNoMoreInteractions(mocks);
    }

    @Test
    void testProcessEventManyDestinations() {
        when(eventRepository.save(eq(event1))).thenReturn(event1);
        var message1 = new SimpleMailMessage();
        message1.setFrom(eventService.getSender());
        message1.setTo(eventService.getDestinations().get(0));
        message1.setSubject("Social network event notification");
        message1.setText(event1.getDescription());
        doNothing().when(javaMailSender).send(eq(message1));
        var message2 = new SimpleMailMessage();
        message2.setFrom(eventService.getSender());
        message2.setTo(eventService.getDestinations().get(1));
        message2.setSubject("Social network event notification");
        message2.setText(event1.getDescription());
        doNothing().when(javaMailSender).send(eq(message2));

        eventService.processEvent(event1);
        verify(javaMailSender).send(eq(message1));
        verify(javaMailSender).send(eq(message2));
        verify(eventRepository).save(eq(event1));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    void testProcessEventManyDestinationsFailed() {
        when(eventRepository.save(eq(event1))).thenReturn(event1);
        var message1 = new SimpleMailMessage();
        message1.setFrom(eventService.getSender());
        message1.setTo(eventService.getDestinations().get(0));
        message1.setSubject("Social network event notification");
        message1.setText(event1.getDescription());
        doThrow(RuntimeException.class).when(javaMailSender).send(eq(message1));
        var message2 = new SimpleMailMessage();
        message2.setFrom(eventService.getSender());
        message2.setTo(eventService.getDestinations().get(1));
        message2.setSubject("Social network event notification");
        message2.setText(event1.getDescription());
        doNothing().when(javaMailSender).send(eq(message2));

        assertThrows(RuntimeException.class, () -> eventService.processEvent(event1));
        verify(javaMailSender).send(eq(message1));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    void testProcessEventNoDestinations() {
        eventService.setDestinations(List.of());
        when(eventRepository.save(eq(event1))).thenReturn(event1);
        doNothing().when(javaMailSender).send(any(SimpleMailMessage.class));
        eventService.processEvent(event1);
        verify(eventRepository).save(eq(event1));
        verifyNoMoreInteractions(mocks);
    }

    @TestConfiguration
    static class EventServiceTestConfiguration {

        @Bean
        public EventService eventService() {
            return new EventService();
        }

    }

}
