package com.getjavajob.training.matiivv.eventnotifier;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataMongoTest
public class EventRepositoryTest {

    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private MongoTemplate mongoTemplate;
    private Event event1 = new Event("event1");
    private Event event2 = new Event("event2");
    private Event event3 = new Event("event3");

    @BeforeEach
    void setUp() {
        event1.setSent(true);
        event1 = mongoTemplate.insert(event1);
        event2 = mongoTemplate.insert(event2);
        event3 = mongoTemplate.insert(event3);
    }

    @Test
    void testFindById() {
        Optional<Event> result = eventRepository.findById(event1.getId());
        assertTrue(result.isPresent());
        assertEquals("event1", result.get().getDescription());
    }

    @Test
    void testFindBySentFalse() {
        List<Event> results = eventRepository.findBySentFalse();
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2, results.size()),
                () -> assertThat(results.stream().map(Event::getDescription).collect(toList()), hasItems("event2", "event3"))
        );
    }

    @Test
    void testCreate() {
        Event event = eventRepository.save(new Event("event"));
        Event result = mongoTemplate.findById(event.getId(), Event.class);
        assertNotNull(result);
        assertEquals("event", result.getDescription());
    }

    @Test
    void testUpdate() {
        event2.setSent(true);
        eventRepository.save(event2);

        Event result = mongoTemplate.findById(event2.getId(), Event.class);
        assertNotNull(result);
        assertTrue(result.isSent());
    }

    @AfterEach
    void tearDown() {
        mongoTemplate.dropCollection(Event.class);
    }

}
