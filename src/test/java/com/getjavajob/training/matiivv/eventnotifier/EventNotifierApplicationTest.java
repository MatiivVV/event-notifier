package com.getjavajob.training.matiivv.eventnotifier;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class EventNotifierApplicationTest {

    @Test
    @SuppressWarnings("EmptyMethod")
    void contextLoads() {
    }

}
