package com.getjavajob.training.matiivv.eventnotifier;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableMongoRepositories
@EnableJms
@EnableScheduling
public class Config {
}
