package com.getjavajob.training.matiivv.eventnotifier;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Document
public class Event {

    @Id
    private String id;
    private String description;
    private boolean sent;

    public Event(String description) {
        this.description = description;
    }

}
