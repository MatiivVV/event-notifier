package com.getjavajob.training.matiivv.eventnotifier;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class EventService {

    private EventRepository eventRepository;
    private JavaMailSender emailSender;
    private EventService eventService;
    private String sender;
    private List<String> destinations;

    public EventRepository getEventRepository() {
        return eventRepository;
    }

    @Autowired
    public void setEventRepository(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public JavaMailSender getEmailSender() {
        return emailSender;
    }

    @Autowired
    public void setEmailSender(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Autowired
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    public String getSender() {
        return sender;
    }

    @Value("${spring.mail.username}")
    public void setSender(String sender) {
        this.sender = sender;
    }

    public List<String> getDestinations() {
        return destinations;
    }

    @Value("${spring.mail.destinations}")
    public void setDestinations(List<String> destinations) {
        this.destinations = destinations;
    }

    @JmsListener(destination = "socialnetwork.event.queue")
    public void receiveEvent(String eventDescription) {
        var event = new Event(eventDescription);
        log.debug("Going to save event {}", event);
        Event result = eventRepository.save(event);
        log.info("Event has been successfully saved {}", result);
    }

    @Scheduled(fixedRate = 10_000)
    public void processEvents() {
        log.debug("Going to fetch events");
        List<Event> events = eventRepository.findBySentFalse();
        if (!events.isEmpty()) {
            log.debug("Going to process events");
            for (Event event : events) {
                try {
                    eventService.processEvent(event);
                } catch (Exception e) {
                    log.warn("Exception {} occurred when processing event {}", e, event);
                }
            }
            log.info("Events have been successfully processed");
        }
    }

    @Transactional
    public void processEvent(Event event) {
        for (String destination : destinations) {
            log.debug("Going to send event {} to {}", event, sender);
            var message = new SimpleMailMessage();
            message.setFrom(sender);
            message.setTo(destination);
            message.setSubject("Social network event notification");
            message.setText(event.getDescription());
            emailSender.send(message);
            log.info("Event {} has been successfully sent to {}", event, sender);
        }
        log.debug("Going to save event status {}", event);
        event.setSent(true);
        Event result = eventRepository.save(event);
        log.info("Event has been successfully processed {}", result);
    }

}
